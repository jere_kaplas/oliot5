package oliot5;

/**
 *
 * @author Kaplas
 */

public class Car
{
    Body body = new Body();
    Chassis chassis = new Chassis();
    Engine engine = new Engine();
    Wheel[] wheel = new Wheel[4];
    
    protected Car()
    {
        for(int i = 0; i < 4; i++)
        {
            wheel[i] = new Wheel();
        }
    }

    void print()
    {
        System.out.printf("Autoon kuuluu:\n\t%s\n\t%s\n\t%s\n\t%d %s\n", this.body.name, this.chassis.name, this.engine.name, this.wheel.length, this.wheel[0].name);
    }
}

class Body
{
    String name = "Body";
    public Body()
    {
        System.out.println("Valmistetaan: Body");
    }
}

class Chassis
{
    String name = "Chassis";
    public Chassis()
    {
        System.out.println("Valmistetaan: Chassis");
    }
}

class Engine
{
    String name = "Engine";
    public Engine()
    {        
        System.out.println("Valmistetaan: Engine");
    }
}
class Wheel
{
    String name = "Wheel";
    public Wheel()
    {
        System.out.println("Valmistetaan: Wheel");
    }
}

